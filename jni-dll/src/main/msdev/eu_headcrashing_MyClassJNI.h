#include <jni.h>

#pragma once

extern "C" JNIEXPORT jint JNICALL Java_eu_headcrashing_MyClassJNI_myMethod
  (JNIEnv *const, const jclass, const jstring);
