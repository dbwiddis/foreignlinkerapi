#include "eu_headcrashing_MyClassJNI.h"

#include <windows.h>

typedef int (__stdcall *LSTRLENW)(const wchar_t*);

HINSTANCE hInstance = NULL;
HINSTANCE lib = NULL;
LSTRLENW lstrlenw = NULL;

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
	switch (fdwReason) {
	case DLL_PROCESS_ATTACH: {
		hInstance = hinstDLL;
		lib = LoadLibraryW(L"Kernel32");
		if (!lib)
			return FALSE;
		lstrlenw = reinterpret_cast<LSTRLENW>(GetProcAddress(lib, "lstrlenW"));
		if (!lstrlenw)
			return FALSE;
		break;
	}
	case DLL_PROCESS_DETACH: {
		lstrlenw = NULL;
		FreeLibrary(lib);
		hInstance = NULL;
		break;
	}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}
	return TRUE;
}

extern "C" JNIEXPORT jint JNICALL Java_eu_headcrashing_MyClassJNI_myMethod(JNIEnv *const env, const jclass cls, const jstring text) {
	const jchar* ptr = env->GetStringChars(text, NULL);
	const int len = lstrlenw(reinterpret_cast<const wchar_t*>(ptr));
	env->ReleaseStringChars(text, ptr);
	return len;
}
