package eu.headcrashing;

public class MyClassJNI {

    static {
        System.loadLibrary("jni-dll-0-SNAPSHOT");
    }

    public static native int myMethod(String text);

}
